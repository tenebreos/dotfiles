alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias l.='ls -d .*'
alias ip='ip -color=auto'
alias fsspace='df -h | grep -v -E "(udev|tmpfs|snap)"'
alias temps='while true; do 
	            clear 
	            sensors | grep --color=none -A4 coretemp-isa-0000
	            sleep 0.5 
	     done'
alias pingscan='nmap -sn 192.168.8.0/24'
alias findssh='nmap 192.168.8.0/24 -p 22 | grep --color=none open -B 1 -A 3'
alias mantoc="grep -E '^([A-Z]+[ ]?)+$' --color=none"
alias info="info --vi-keys"
alias ssh="kitty +kitten ssh"
alias icat="kitty +kitten icat"
alias neofetch="neofetch --ascii_distro=arch"

if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	alias dir='dir --color=auto'
	alias vdir='vdir --color=auto'

	alias diff='diff --color=auto'
	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'

	# This set allows for colors in a pager and in pipes, but gets annoying
	# when using their output as arguments to some commands
	# alias ls='ls --color=always'
	# alias diff='diff --color=always'
	# alias grep='grep --color=always'
	# alias fgrep='fgrep --color=always'
	# alias egrep='egrep --color=always'
	# alias less='less -r'
fi
