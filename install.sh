#!/bin/sh

# install.sh: install the dotfiles

# TODO: make this bulletproof.

ln -s ~/.dotfiles/vim/ ~/.vim
ln -s ~/.dotfiles/inputrc ~/.inputrc
ln -s ~/.dotfiles/tmux.conf ~/.tmux.conf
ln -s ~/.dotfiles/clang-format ~/.clang-format
ln -s ~/.dotfiles/my.cnf ~/.my.cnf
mkdir -p ~/.config/nvim/
ln -s ~/.dotfiles/init.vim ~/.config/nvim/init.vim
echo "source ~/.dotfiles/bashrc" >> ~/.bashrc
