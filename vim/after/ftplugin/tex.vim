source ~/.vim/after/ftplugin/indent_8space.vim
source ~/.vim/after/ftplugin/lang_default.vim

let g:tex_fold_enabled = 1
setlocal spell spelllang=pl,en_gb
setlocal foldmethod=syntax
" setlocal makeprg=xelatex\ %:S
setlocal makeprg=xelatex\ -shell-escape\ %:S
map <buffer> <F9> ;execute 'silent !evince %:r.pdf & disown' <bar> redraw!<cr>
highlight link SpellBad Error
