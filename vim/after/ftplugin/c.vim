source ~/.vim/after/ftplugin/lang_default.vim
source ~/.vim/after/ftplugin/lsp_default.vim
source ~/.vim/after/ftplugin/indent_8space.vim

setlocal cindent cino=:0
setlocal colorcolumn=80

compiler gcc
setlocal makeprg=gcc\ -g\ -o\ %:r:S\ %:S
setlocal keywordprg=:Man

map <buffer> <F7> ;execute 'silent !ddd %:r:S & disown' <bar> redraw!<CR>
map <buffer> <F9> ;!rlwrap %:p:r:S<up><CR>
map <buffer> <S-F9> ;!rlwrap %:p:r:S 

setlocal path=.,**,/usr/include/**

