source ~/.vim/after/ftplugin/lang_default.vim
source ~/.vim/after/ftplugin/lsp_default.vim
source ~/.vim/after/ftplugin/indent_4space.vim

setlocal cindent cinoptions=j1,:0
setlocal colorcolumn=80

setlocal omnifunc=lsp#complete

compiler javac
setlocal makeprg=javac\ %:S

map <buffer> <F9> ;!java %:r:S<up><CR>
map <buffer> <S-F9> ;!java %:r:S 
"## Syntax:"
"autocmd FileType java syn match javaClassType /\v<[A-Z][A-Za-z0-9_]*>/
"autocmd FileType java syn match javaClassType /\v<[A-Z][A-Za-z0-9_]*%(\<)@=/
"autocmd FileType java syn match javaConst /\v<[A-Z_]+>/
"autocmd FileType java syn keyword javaTypeVariable T U R K V E S
"autocmd FileType java syn cluster javaTop add=javaClassType
"autocmd FileType java syn cluster javaTop add=javaTypeVariable
"autocmd FileType java hi link javaConst Constant
"autocmd FileType java hi link javaClassType Type
" TODO: move the syntax definitions to a seperate file
"let java_highlight_functions="style" // Takes precedence over typenames, not desired

"## Abreviations:"
abbreviate pcls public class
abbreviate psv public static void
abbreviate sof System.out.format
abbreviate sout System.out.println
abbreviate sop System.out.println
abbreviate serr System.err.println
abbreviate psvm public static void main(String[] argv) {
