source ~/.vim/after/ftplugin/lang_default.vim
source ~/.vim/after/ftplugin/lsp_default.vim
source ~/.vim/after/ftplugin/indent_4space.vim

setlocal colorcolumn=80

setlocal keywordprg=pydoc3

nmap <buffer> <F9> ;!python3 %:S<up><CR>
nmap <buffer> <S-F9> ;!python3 %:S 

setlocal path=.,**
setlocal wildignore=*.pyc,*/__pycache__/*

set include=^\\s*\\(from\\\|import\\)\\s*\\zs\\(\\S\\+\\s\\{-}\\)*\\ze\\($\\\|\ as\\)

" import foo.bar
" /sys.argv/
" foo/bar.py
"
" from conv import conversion as conv
" /conv import conversion/
" conv/conversion.py conv.py
function! PyInclude(fname)
    let parts = split(a:fname, ' import ')
    let l = parts[0]
    if len(parts) > 1
        let r = parts[1]
        let joined = join([l, r], '.')
        let fp = substitute(joined, '\.', '/', 'g') . 'py'
        let found = glob(fp, 1)
        if len(found)
            return found
        endif
    endif
    return substitute(l, '\.', '/', 'g') . 'py'
endfunction
setlocal includeexpr=PyInclude(v:fname)
setlocal define=^\\s*\\<\\(def\\\|class\\)\\>

"'set include?' lists the regex defining include lines in source code files.
"Word that is a name of file to search is taken as the next word after the pattern.
"'set suffixesadd?' lists extensions which will be added to the name of the
"file to search.
"'set includeexpr' lists an expression which will return a name of a file to
"search for based on the name found by the 'set include' regex.

"':ij <symbol>' jumps to symbols based on the include search mechanism
