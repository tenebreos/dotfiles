source ~/.vim/after/ftplugin/lang_default.vim

compiler go
nnoremap <buffer> <F8> :silent make <bar> redraw!<CR>
