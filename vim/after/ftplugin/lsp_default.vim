nnoremap <buffer> <leader>i :LspDocumentFormat<cr>
nnoremap <buffer> <leader>R :LspRename<cr>
nnoremap <buffer> <leader>r :LspReferences<cr>
nnoremap <buffer> <leader>p :LspPeekDefinition<cr>
nnoremap <buffer> <leader>P :LspDefinition<cr>
nnoremap <buffer> <leader>t :Vista!!<cr>
nnoremap <buffer> <leader>d :LspDocumentDiagnostics<cr>

setlocal omnifunc=lsp#complete
setlocal foldmethod=expr
       \ foldexpr=lsp#ui#vim#folding#foldexpr()
       \ foldtext=lsp#ui#vim#folding#foldtext()
