source ~/.vim/after/ftplugin/lang_default.vim

setlocal keywordprg=:Man

map <buffer> <F8> ;!shellcheck ./%:S; chmod u+x ./%:S<CR>
map <buffer> <F9> ;!bash ./%:S<up><CR>
map <buffer> <S-F9> ;!bash ./%:S 
