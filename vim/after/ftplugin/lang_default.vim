nnoremap <buffer> <F8> :silent make <bar> redraw!<CR>
nnoremap <buffer> <S-F8> ;let tmp_makeprg=&l:makeprg<bar>
                        \ setl makeprg=make<bar>
                        \ make<bar>
                        \ let &l:makeprg=tmp_makeprg<cr>
